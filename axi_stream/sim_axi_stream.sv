`timescale  1 ns/1 ps

module sim_axi_stream ();
`include "axi_stream_bfm.svh"
`include "axi_stream_drive.svh"

reg                     clk = 0;
always
    #(1s/100_000_000/2) clk = ~clk;

reg                     rst = 0;
initial
begin
    rst = 0; #1us;
    rst = 1; #1us; 
    rst = 0; #1us; 
end

axi_stream_bfm #
(
    .WIDTH  (8)
)
axi_stream_if 
(
    .s_aclk       (    clk      ),
    .s_aresetn    (    ~rst     )
);

//axi_stream_drive #(.width (8) ) axi_stream_drive_h;
axi_stream_drive #(8) axi_stream_drive_h;

int frame[];
initial begin
    axi_stream_drive_h = new(axi_stream_if);

    frame = new[4];
    for(int i = 0; i < 4; i = i + 1 )
    begin
        frame[i] = i+1;
    end
    #3us; axi_stream_drive_h.write(frame, 4, TRUE);
    #1us; axi_stream_drive_h.write(frame, 4, FALSE);
    #1us; axi_stream_drive_h.write(frame, 4, FALSE);
end

 initial
 begin
     axi_stream_if.m_axis_tready = 0;
     #3us;
     @ (posedge clk);
     @ (posedge clk);
     @ (posedge clk);
     @ (posedge clk);
     axi_stream_if.m_axis_tready = 1;
     @ (posedge clk);
     axi_stream_if.m_axis_tready = 0;
 
     @ (posedge clk);
     @ (posedge clk);
     @ (posedge clk);
     @ (posedge clk);
     axi_stream_if.m_axis_tready = 1;
     @ (posedge clk);
     axi_stream_if.m_axis_tready = 0;
 
     @ (posedge clk);
     @ (posedge clk);
     @ (posedge clk);
     @ (posedge clk);
     axi_stream_if.m_axis_tready = 1;
     @ (posedge clk);
     axi_stream_if.m_axis_tready = 0;
 
     @ (posedge clk);
     @ (posedge clk);
     @ (posedge clk);
     @ (posedge clk);
     axi_stream_if.m_axis_tready = 1;
     @ (posedge clk);
     axi_stream_if.m_axis_tready = 0;
 
     @ (posedge clk);
     @ (posedge clk);
     @ (posedge clk);
     @ (posedge clk);
     axi_stream_if.m_axis_tready = 1;
     @ (posedge clk);
     axi_stream_if.m_axis_tready = 0;
 end
endmodule
