/*=============================================================================
# FileName    :	axi_stream_bfm.svh
# Author      :	author
# Email       :	email@email.com
# Description :	data端口使用32位，如果实际没有用到，例化的时候去掉
# Version     :	1.0
# LastChange  :	2018-09-17 16:57:04
# ChangeLog   :	
=============================================================================*/

interface axi_stream_bfm #
(
    parameter WIDTH = 32
)
(
    input                               s_aclk,
    input                               s_aresetn
);

    logic                               m_axis_tvalid;
    logic                               m_axis_tlast;
    logic                               m_axis_tready;  // slave控制此端口
    logic [WIDTH-1:00]                  m_axis_tdata;

    logic                               s_axis_tvalid;
    logic                               s_axis_tlast;
    logic                               s_axis_tready;
    logic [WIDTH-1:00]                  s_axis_tdata;

    // 使用modport的模块端口方向和modport里声明的一致
    modport master
    (
        input                       s_aclk, s_aresetn, m_axis_tready,
        output                      m_axis_tvalid, m_axis_tlast, m_axis_tdata
    );

    modport slave
    (
        input                       s_aclk, s_aresetn, s_axis_tvalid, s_axis_tlast, s_axis_tdata,
        output                      s_axis_tready
    );

    initial
    begin
        m_axis_tvalid = 0;
        m_axis_tlast = 0;
        m_axis_tdata = 0;

        s_axis_tready = 0;

    end

endinterface

typedef enum bit[02:00]
{
    INC = 3'b000,
    DEC = 3'b001,
    RANDOM = 3'b010 
}DATA_PATTERN;

typedef enum bit
{
    TRUE = 1'b1,
    FALSE = 1'b0
}e_use_tready;


