quit -sim
# 可以不手动建立library
#vlib    work
#vmap    work work

#工程所需要的文件
vlog -sv -incr ./sim_axi_stream.sv

vsim -t ns -voptargs="+acc" work.sim_axi_stream

log -r /*
radix 16

do wave.do

run 20us
