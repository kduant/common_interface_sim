quit -sim
# 可以不手动建立library
#vlib    work
#vmap    work work


#工程所需要的文件
vlog -sv -incr ./uart_tb.sv
vlog -incr ../../../study/hdl_example/common/uart/rx_part.v
vlog -incr ../../../study/hdl_example/common/uart/tx_part.v
vlog -incr ../../../study/hdl_example/common/uart/uart.v

vsim -t ps -voptargs="+acc" work.uart_tb

log -r /*
radix 16

do wave.do

run 100ms
