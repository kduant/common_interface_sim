`timescale  1 ns/1 ps

module uart_tb ();

`include "uart_bfm.svh"
`include "uart_drive.svh"

reg                     clk = 0;
always
    #(1s/125_000_000/2) clk = ~clk;

reg                     rst = 0;
initial
begin
    rst = 0; #1us;
    rst = 1; #1us; 
    rst = 0; #1us; 
end

initial
begin            
    $dumpfile("wave.vcd");        //生成的vcd文件名称
    $dumpvars(0, uart_tb);    //tb模块名称
end

uart_bfm #
(
    .BAUDRATE  (460800                        )
)
uart_bfm_if ();

uart_drive #(460800) uart_drive_h;


byte data[];
initial 
begin
    uart_drive_h = new(uart_bfm_if);


    data = new[1024];
    for(int i = 0; i < $size(data); i = i + 1 )
    begin
        data[i] = i;
    end

    #200us uart_drive_h.write(data);
end



uart #
(
    .SYS_CLK_FREQ    (  125_000_000     ),
    .BAUDRATE        (  460800          )
)
uartEx01
(
    .clk             (  clk                 ),
    .rxd             (  uart_bfm_if.txd    ),
    .rd_data         (                      ),
    .rd_valid        (                      ),
    .wr_req          (                      ),
    .wr_data         (                      ),
    .txd             (                      ),
    .tx_status       (                      )
);
endmodule
