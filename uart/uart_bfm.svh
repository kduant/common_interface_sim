interface uart_bfm #
(
    parameter   BAUDRATE = 115200
)
();
    logic                       rxd;
    logic                       txd = 1;
    // 
    int data_len = 8;
    int parity = 0;
    int stop_bit = 0;
    initial
    begin
        txd = 1;
    end
endinterface: uart_bfm
    
