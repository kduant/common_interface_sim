interface sata_bfm
(
);
    logic                       sata_tx;

    logic                       sata_rx;

    // 使用modport的模块端口方向和modport里声明的一致
    modport host
    (
        output                      sata_tx
    );

    modport device
    (
        input                       sata_rx
    );
    
typedef enum 
{
    COMRESET = 1,   // host send
    COMINIT,        // device send
    COMWAKE         // both send

}OobCommand;

endinterface
