`ifndef SATA_DRIVE_SVH
`define SATA_DRIVE_SVH
// 1.5Gbps/0.667ns

class sata_drive;
    virtual sata_bfm bfm;
    /*
     * 静态属性
     */
    function new(virtual sata_bfm b);  // 需要赋初值的属性
        bfm = b;
    endfunction

    extern task write_oob_command(OobCommand command);
    extern task write_oob_primitive();
    extern task write_oob_idle(float idle_time);
endclass


task sata_drive::write_oob_command(OobCommand command)
    case (command)
        COMRESET:
            begin
                repeat(6)
                begin
                    write_oob_primitive();
                    write_oob_idle(320);
                end
            end
        COMINIT:
            begin
                repeat(6)
                begin
                    write_oob_primitive();
                    write_oob_idle(320);
                end
            end
        COMWAKE:
            begin
                repeat(6)
                begin
                    write_oob_primitive();
                    write_oob_idle(106.7);
                end
            end

    endcase
endtask

// oob原语数据：D24.3(推荐)/ALIGN(允许)
// D24.3 = 1100 1100 1100 1100 1100
// 20*0.667 = 13.34 * 8 = 106.72
task sata_drive::write_oob_primitive()
    for(int i = 0; i < 40; i = i + 1 )
    begin
        bfm.tx = 1; #1.33335ns;
        bfm.tx = 0; #1.33335ns;
    end
endtask

task sata_drive::write_oob_idle(float idle_time)
    #idle_time ns;
`endif
