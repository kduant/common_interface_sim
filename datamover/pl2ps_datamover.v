/* =============================================================================
# FileName    :	pl2ps_datamover.v
# Author      :	author
# Email       :	email@email.com
# Description :	
                TODO: 
                    1. 数据
                    2. 在某个地址范围内循环写入数据
# Version     :	1.0
# LastChange  :	2024-11-19 17:07:25
# ChangeLog   :	
============================================================================= */

`timescale  1 ns/1 ps

module pl2ps_datamover #
(
    parameter               BASE_ADDR = 32'h3000000,
    parameter               WIDTH = 32
)
(
    input                       clk,

    input                       s_axis_tvalid,
    output                      s_axis_tready,
    input        [WIDTH-1:00]   s_axis_tdata,
    input                       s_axis_tlast,
    input        [31:00]        s_axis_tlen,

    output                      write_over_flag,

    input       [07:00]         S_AXIS_S2MM_STS_tdata,
    input       [00:00]         S_AXIS_S2MM_STS_tkeep,
    input                       S_AXIS_S2MM_STS_tlast,
    output                      S_AXIS_S2MM_STS_tready,
    input                       S_AXIS_S2MM_STS_tvalid,
    

    output      [WIDTH-1:00]    M_AXIS_S2MM_tdata,
    output      [03:00]         M_AXIS_S2MM_tkeep,
    output                      M_AXIS_S2MM_tlast,
    input                       M_AXIS_S2MM_tready,
    output                      M_AXIS_S2MM_tvalid,

    output      [71:00]         M_AXIS_S2MM_CMD_tdata,
    input                       M_AXIS_S2MM_CMD_tready,
    output                      M_AXIS_S2MM_CMD_tvalid
);

reg     [WIDTH-1:0]         m_axis_tdata = 0;
reg     [3:0]               m_axis_tkeep = 4'b1111;
reg                         m_axis_tlast = 0;
reg                         m_axis_tvalid = 0;

reg     [71:00]             s2mm_cmd_tdata = 0;
reg                         s2mm_cmd_tvalid = 0;

wire    [71:00]             cmd_data;
reg     [22:00]             write_bytes = 0;

assign                  cmd_data = {
    8'd0,
    BASE_ADDR,
    1'b0,   // DRR
    1'b1,   // EOF
    6'd0,   // DSA
    1'b1,   // TYPE,  1:incr 0:fixed
    write_bytes[22:00]  // BTT , 一次传输的字节数
    };

localparam              IDLE    = 0;
localparam              MATCH   = 1;   // 设置cmd_data 
localparam              CMD     = 2;
localparam              DLY     = 3;
localparam              DATA    = 4;
localparam              OVER    = 5;
(* KEEP = "TRUE" *)reg     [OVER:00]       cs = 'd1, ns = 'd1;
reg     [15:00]         state_cnt = 0;
reg     [15:00]         state_cnt_n = 0;

// synthesis translate_off
reg [127:0] cs_STRING;
always @(*)
begin
    case(1'b1)
        cs[IDLE]: cs_STRING = "IDLE";
        cs[MATCH]: cs_STRING = "MATCH";
        cs[CMD]: cs_STRING = "CMD";
        cs[DLY]: cs_STRING = "DLY";
        cs[DATA]: cs_STRING = "DATA";
        cs[OVER]: cs_STRING = "OVER";
        default: cs_STRING = "XXXX";
    endcase
end
// synthesis translate_on

always @(posedge clk)
begin
    cs <= ns;
end

always @(*)
begin
    ns = 'd0;
    case(1'b1)
        cs[IDLE]:
        begin
            if(s_axis_tvalid)
                ns[MATCH] = 1'b1;
            else
                ns[IDLE] = 1'b1;
        end
        cs[MATCH]:
        begin
            if(state_cnt == 4)
                ns[CMD] = 1'b1;
            else
                ns[MATCH] = 1'b1;
        end
        cs[CMD]:
        begin
            if(M_AXIS_S2MM_CMD_tready)
                ns[DLY] = 1'b1;
            else
                ns[CMD] = 1'b1;
        end
        cs[DLY]:
        begin
            if(state_cnt == 16'd4)
                ns[DATA] = 1'b1;
            else
                ns[DLY] = 1'b1;
        end
        cs[DATA]:
        begin
            if(s_axis_tvalid & s_axis_tready & s_axis_tlast)
                ns[OVER] = 1'b1;
            else
                ns[DATA] = 1'b1;
        end
        cs[OVER]:
        begin
            ns[IDLE] = 1'b1;
        end
        default:
            ns[IDLE] = 1'b1;
    endcase
end

always @ (posedge clk)
begin
    state_cnt <= state_cnt_n;
end

always @ (*)
begin
    if (cs != ns)
        state_cnt_n = 0;
    else
        state_cnt_n = state_cnt + 1'b1;
end


always @ (posedge clk)
begin
    if(cs[MATCH] & ns[CMD])
        s2mm_cmd_tvalid <= 1'b1;
    else if(M_AXIS_S2MM_CMD_tready)
        s2mm_cmd_tvalid <= 1'b0;
end
always @ (posedge clk)
begin
    if(cs[MATCH] & ns[CMD])
        s2mm_cmd_tdata <= cmd_data;
    else if(cs[DATA] & ns[OVER])
        s2mm_cmd_tdata <= 0;
end

always @ (posedge clk)
begin
    if(cs[IDLE] & ns[MATCH])
        write_bytes <= (s_axis_tlen << 2);
end

always @ (*)
begin
    m_axis_tvalid = s_axis_tvalid & s_axis_tready;
end

always @ (*)
begin
    m_axis_tdata = s_axis_tdata;
end

always @ (*)
begin
    m_axis_tlast <= s_axis_tlast;
end

assign                  write_over_flag = m_axis_tlast & m_axis_tvalid;
assign                  s_axis_tready = cs[DATA] & M_AXIS_S2MM_tready;

assign                  M_AXIS_S2MM_CMD_tvalid = s2mm_cmd_tvalid;
assign                  M_AXIS_S2MM_CMD_tdata = s2mm_cmd_tdata;

assign                  M_AXIS_S2MM_tvalid = m_axis_tvalid;
assign                  M_AXIS_S2MM_tdata = m_axis_tdata;
assign                  M_AXIS_S2MM_tlast = m_axis_tlast;
assign                  M_AXIS_S2MM_tkeep = 4'b1111;
assign                  S_AXIS_S2MM_STS_tready = 1'b1;

endmodule

