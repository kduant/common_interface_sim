`timescale  1 ns/1 ps

module sim_datamover ();
reg                     clk = 0;
always
    #(1s/100_000_000/2) clk = ~clk;

logic                       s_axis_tvalid = 0;
logic                       s_axis_tready;
logic                       s_axis_tlast = 0;
logic   [31:00]             s_axis_tdata = 0;
logic   [31:00]             s_axis_tlen = 16;

initial
begin
    #2us;
    @(posedge clk);
    s_axis_tvalid = 1;
    wait(s_axis_tready);
    for(int i = 0; i < 16; i++)
    begin
        s_axis_tdata = i;
        s_axis_tlast = (i == 15);
        @(posedge clk);
        wait(s_axis_tready);
    end
    s_axis_tvalid = 0;
    s_axis_tlast = 0;
end


pl2ps_datamover #
(
    .BASE_ADDR                   (  32'h3000_0000               ),
    .WIDTH                       (  32                          )
)
pl2ps_datamoverEx01
(
    .clk                         (  clk                             ),
    .s_axis_tvalid               (  s_axis_tvalid                   ),
    .s_axis_tready               (  s_axis_tready                   ),
    .s_axis_tdata                (  s_axis_tdata                    ),
    .s_axis_tlast                (  s_axis_tlast                    ),
    .s_axis_tlen                 (  s_axis_tlen                     ),
    .write_over_flag             (                                  ),
    .S_AXIS_S2MM_STS_0_tdata     (                                  ),
    .S_AXIS_S2MM_STS_0_tkeep     (                                  ),
    .S_AXIS_S2MM_STS_0_tlast     (                                  ),
    .S_AXIS_S2MM_STS_0_tready    (                                  ),
    .S_AXIS_S2MM_STS_0_tvalid    (                                  ),
    .M_AXIS_S2MM_0_tdata         (                                  ),
    .M_AXIS_S2MM_0_tkeep         (                                  ),
    .M_AXIS_S2MM_0_tlast         (                                  ),
    .M_AXIS_S2MM_0_tready        (  1'b1            ),
    .M_AXIS_S2MM_0_tvalid        (                                  ),
    .M_AXIS_S2MM_CMD_0_tdata     (                                  ),
    .M_AXIS_S2MM_CMD_0_tready    (  1'b1        ),
    .M_AXIS_S2MM_CMD_0_tvalid    (                                  )
);
endmodule
