/*=============================================================================
# FileName    :	axi4_bfm.svh
# Author      :	author
# Email       :	email@email.com
# Description :	
                // x 表示 axi4-lite不支持此信号
# Version     :	1.0
# LastChange  :	2018-04-05 18:26:40
# ChangeLog   :	
=============================================================================*/
//interface axi4_bfm #
//(
    //S_AXI_ADDR_WIDTH = 32
//);

interface axi4_bfm;
    logic                               s_aclk;
    logic                               s_aresetn;

    logic   [00:00]                     s_axi_awlock;       // x
    logic   [03:00]                     s_axi_awcache;      // x
    logic   [02:00]                     s_axi_awprot;       // x
    logic   [03:00]                     s_axi_awid;         // x
    logic   [07:00]                     s_axi_awlen;        // x
    logic   [02:00]                     s_axi_awsize;       // x
    logic   [01:00]                     s_axi_awburst;      // x

    logic   [31:00]                     s_axi_awaddr;
    logic                               s_axi_awvalid;
    logic                               s_axi_awready;

    logic   [03:00]                     s_axi_wid;          // x
    logic   [31:00]                     s_axi_wdata;
    logic   [03:00]                     s_axi_wstrb;
    logic                               s_axi_wvalid;
    logic                               s_axi_wready;
    logic                               s_axi_wlast;        // x

    logic   [03:00]                     s_axi_bid;          // x
    logic   [01:00]                     s_axi_bresp;
    logic                               s_axi_bvalid;
    logic                               s_axi_bready;

    logic   [00:00]                     s_axi_arlock;       // x
    logic   [03:00]                     s_axi_arcache;      // x
    logic   [02:00]                     s_axi_arprot;       // x
    logic   [03:00]                     s_axi_arid;         // x
    logic   [07:00]                     s_axi_arlen;        // x
    logic   [02:00]                     s_axi_arsize;       // x
    logic   [01:00]                     s_axi_arburst;      // x
    logic   [31:00]                     s_axi_araddr;
    logic                               s_axi_arvalid;
    logic                               s_axi_arready;

    logic   [03:00]                     s_axi_rid;          // x
    logic   [01:00]                     s_axi_rresp;
    logic   [31:00]                     s_axi_rdata;
    logic                               s_axi_rvalid;
    logic                               s_axi_rready;
    logic                               s_axi_rlast;        // x

    initial
    begin
        s_aresetn = 1; #100ns;
        s_aresetn = 0; # 1us;
        s_aresetn = 1;
    end
    initial
    begin
        s_aclk = 0;
        forever
        begin
            #(1s/100_000_000/2) s_aclk = ~s_aclk;
        end
    end
endinterface: axi4_bfm
/*
1. AWLEN[3:0]
    一次突发事务中传输的单位个数.

AWLEN[3:0]   |  传输单位个数
-------------|-------------
b0000        | 1
b0001        | 2
b0010        | 3
b0011        | 4
b0100        | 5
b0101        | 6
b0110        | 7
b0111        | 8
b1000        | 9
b1001        | 10
b1010        | 11
b1011        | 12
b1100        | 13
b1101        | 14
b1110        | 15
b1111        | 16

2. AWSIZE[2:0]
    每个单位的宽度. 例如32bit传输时，AWSIZE = `3'b010`

AWSIZE[2:0]   |  字节数
--------------|-------------
b000          | 1
b001          | 2
b010          | 4
b011          | 8
b100          | 16
b101          | 32
b110          | 64
b111          | 128

3. AWBURST[1:0]，常用的2种模式
    * `2'b00` 固定地址模式，用于FIFO访问
    * `2'b01` 地址自增模式，用户线性存储器
    
4. ID
ID通常在乱序或者一主多从中使用


# AXI4，AXI4-Lite区别
信号                |  AXI4        | AXI4-Lite
--------------------|--------------|---------------
ACLK                | 支持         |            
ARESETn             | 支持         |                                          
AWID[3:0]           | 支持         |
AWADDR[31:0]        | 支持         |
AWLEN[3:0]          | 支持         | 不支持
AWSIZE[2:0]         | 支持         | 不支持
AWBURST[1:0]        | 支持         | 不支持
AWLOCK[1:0]         | 支持         | 不支持
AWCACHE[3:0]        | 支持         | 不支持
AWPROT[2:0]         | 支持         |
AWVALID             | 支持         |
AWREADY             | 支持         |
WID[3:0]            | 支持         | 不支持
WDATA[31:0]         | 支持         |
WSTRB[3:0]          | 支持         |
WLAST               | 支持         | 不支持
WVALID              | 支持         |
WREADY              | 支持         |
BID[3:0]            | 支持         | 不支持
BRESP[1:0]          | 支持         |
BVALID              | 支持         |
BREADY              | 支持         |
ARID[3:0]           | 支持         | 不支持
ARADDR[31:0]        | 支持         |
ARLEN[3:0]          | 支持         | 不支持
ARSIZE[2:0]         | 支持         | 不支持
ARBURST[1:0]        | 支持         | 不支持
ARLOCK[1:0]         | 支持         | 不支持
ARCACHE[3:0]        | 支持         | 不支持
ARPROT[2:0]         | 支持         |
ARVALID             | 支持         |
ARREADY             | 支持         |
RID[3:0]            | 支持         | 不支持
RDATA[31:0]         | 支持         |
RRESP[1:0]          | 支持         |
RLAST               | 支持         | 不支持
RVALID              | 支持         |
RREADY              | 支持         |
*/
