`ifndef KEY_DRIVER_SVH
`define KEY_DRIVER_SVH

class key_driver;
    virtual key_bfm bfm;
    /*
     * 静态属性
     */
    int shake_time;
    int noise_time;
    function new(virtual key_bfm b);  // 需要赋初值的属性
        bfm = b;
    endfunction

    extern task key_config(int shake_time, int noise_time);
    extern task tremble();
endclass

task key_driver::key_config(int shake_time, int noise_time);
    this.shake_time = shake_time;
    this.noise_time = noise_time;
endtask

task key_driver::tremble();
    int s = shake_time / 8;
    int n = noise_time / 8;
    int c;

    while(s > 0)
    begin
        c = $urandom_range(0, n);
        bfm.key = c % 2;;
        repeat(c)
        begin
            @ (posedge bfm.clk);
        end
        s = s - c;
        $display("c = %d \n", c);
        $display("s = %d \n", s);
    end
endtask

task key_driver::key_release();
endtask

`endif
