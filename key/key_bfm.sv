interface key_bfm ();
    logic                       key;
    logic                       clk = 0;

    always
        #(1s/125_000_000/2) clk = ~clk;

    initial
    begin
        key = 0;
    end

endinterface
