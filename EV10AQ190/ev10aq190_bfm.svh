interface ev10aq190_bfm;
    logic                       trg_in;
    logic                       adc_clk;

    logic [9:0]                 adc_din;

    initial
    begin
        adc_clk = 0;
        forever 
        begin
            //#(1s/500_000_000/2); 
            //#(1s/499_999_990/2); 
            #(1000ps); 
            adc_clk = ~adc_clk;
        end
    end

endinterface

