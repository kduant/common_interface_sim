/*=============================================================================
# FileName    :	axi4_lite_bfm.svh
# Author      :	author
# Email       :	email@email.com
# Description :	
                // x 表示 axi4-lite不支持此信号
# Version     :	1.0
# LastChange  :	2023-07-18 13:26:31
# ChangeLog   :	
=============================================================================*/

interface axi4_lite_bfm 
(
    input                               s_aclk,
    input                               s_aresetn
);
    logic   [31:00]                     s_axi_awaddr;
    logic   [02:00]                     s_axi_awprot;
    logic                               s_axi_awvalid;
    logic                               s_axi_awready;

    logic   [31:00]                     s_axi_wdata;
    logic   [03:00]                     s_axi_wstrb;
    logic                               s_axi_wvalid;
    logic                               s_axi_wready;

    logic   [01:00]                     s_axi_bresp;
    logic                               s_axi_bvalid;
    logic                               s_axi_bready;

    logic   [31:00]                     s_axi_araddr;
    logic                               s_axi_arvalid;
    logic                               s_axi_arready;
    logic   [02:00]                     s_axi_arprot;

    logic   [01:00]                     s_axi_rresp;
    logic   [31:00]                     s_axi_rdata;
    logic                               s_axi_rvalid;
    logic                               s_axi_rready;

    // initial
    // begin
        // s_aresetn = 1; #100ns;
        // s_aresetn = 0; # 1us;
        // s_aresetn = 1;
    // end
    // initial
    // begin
        // s_aclk = 0;
        // forever
        // begin
            // #(1s/100_000_000/2) s_aclk = ~s_aclk;
        // end
    // end
endinterface: axi4_lite_bfm
