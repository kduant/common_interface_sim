`ifndef BRAM_DRIVE_H
`define BRAM_DRIVE_H

class bram_drive ;
    virtual bram_bfm   bfm;

    function new(virtual bram_bfm b);  // 需要赋初值的属性
        bfm = b;
    endfunction

    extern task write(int addr, int data);
    // extern task read(int addr);
endclass

task bram_drive::write(int addr, int data);
    @ (posedge bfm.mmap_clk);
    bfm.mmap_addr = addr;
    @ (posedge bfm.mmap_clk);
    @ (posedge bfm.mmap_clk);
    @ (posedge bfm.mmap_clk);
    bfm.mmap_en = 1'b1;
    bfm.mmap_we = 4'hf;
    bfm.mmap_dout = data;
    @ (posedge bfm.mmap_clk);
    bfm.mmap_addr = 0;
    bfm.mmap_en = 1'b0;
    bfm.mmap_we = 4'h0;
endtask
`endif
