interface bram_bfm 
(
    logic                       mmap_clk,
    logic                       mmap_rst
);

logic                       mmap_en;
logic        [03:00]        mmap_we;
logic        [15:00]        mmap_addr; 
logic        [31:00]        mmap_din;
logic        [31:00]        mmap_dout;


modport master
(
    input                       mmap_din,
    output                      mmap_en, mmap_we, mmap_addr, mmap_dout
);

modport slave
(
    output                      mmap_din,
    input                       mmap_en, mmap_we, mmap_addr, mmap_dout
);

initial
begin
    mmap_en = 0;
    mmap_we = 0;
    mmap_addr = 0;
    // mmap_din = 0;
    mmap_dout = 0;
end
endinterface
