interface spi_bfm # 
(
    parameter               WIDTH = 8,
    parameter               FREQ = 1_000_000
)
();
logic                       sclk;
logic                       scs;
logic                       mosi;
logic                       miso;
/*
 * bit[2] : lsbfe;  1, MSB first; 0, LSB first
 *
 * bit[7]   bit[6]   bit[5]   bit[4]   bit[3]   bit[2]   bit[1]   bit[0]
                                                lsbfe    cpol     cpha
 */
logic   [07:00]             config_reg;

initial
begin
    scs = 1;
    sclk = 0;
    mosi = 0;
    config_reg = 8'h00;
end

// 模拟spi master发送数据, 测试spi slave功能
//modport master
//(
    //input                       sclk, scs, mosi,
    //output                      miso
//);
endinterface

