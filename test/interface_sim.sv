`timescale  1 ns/1 ps

module tttt
    (
        output  led
    );

    wire    w0;
    reg     r0 = 0;
    assign                  led = r0;

endmodule

module interface_sim ();

`define PRINT_UART_INFO
//`include "../spi/spi_bfm.svh"
//`include "../spi/spi_drive.svh"
//`include "../axi4/axi4_bfm.svh"
//`include "../axi4/axi4_drive.svh"
//`include "../uart/uart_bfm.svh"
//`include "../uart/uart_drive.svh"
//`include "../axi_stream/axi_stream_bfm.svh"
//`include "../axi_stream/axi_stream_drive.svh"

//`include "axi_stream_bfm.svh"
//`include "axi_stream_drive.svh"
//`include "../motor/motor_bfm.sv"
//`include "../motor/motor_driver.sv"

`include "../key/key_bfm.sv"
`include "../key/key_driver.sv"

reg                     clk = 0;
always
    #(1s/1_000_000/2) clk = ~clk;

reg                     rst_n = 1;
initial
begin
    #1us; rst_n = 0;
    #1us; rst_n = 1;
end
key_bfm key_bfm_if();
key_driver key_driver_h;

initial
begin
    key_driver_h = new(key_bfm_if);
    key_driver_h.key_config(300_000, 10_000);
    #100us;
    key_driver_h.key_press;
end


endmodule
