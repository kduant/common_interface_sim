quit -sim
vlog -sv -incr interface_sim.sv

vsim -t ps -voptargs="+acc" work.interface_sim

log -r /*
radix 16

do wave.do

run 20us
