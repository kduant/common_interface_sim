
提供常用接口相关的task

# AXI4
## task
* task write_config();
    配置写操作的大小和突发长度
* task write_dword(int addr, bit [31:00] data);
    向`addr`写入`data`
* task write_burst(int addr, int len);
    向`addr`写入随机数据
* task read_dword(int addr);
    读取`addr`地址的值
* task read_burst(int addr, int len);

## example
```verilog
`include "./axi_sim/axi4_bfm.svh"
`include "./axi_sim/axi4_drive.svh"

axi4_bfm axi4_bfm_i();
axi4_drive axi4_drive_h;

initial
begin
    axi4_drive_h = new(axi4_bfm_i);
    axi4_drive_h.init();
    axi4_drive_h.write_config();
    axi4_drive_h.read_config();

    #2us axi4_drive_h.write_burst(4, 5);
    #2us axi4_drive_h.read_burst(4, 4);
    //#2us axi4_drive_h.write_dword(0, 32'h11223344);
    //#2us axi4_drive_h.write_dword(8, 32'h55223344);
    //#1us axi4_drive_h.read_dword(0);
    //#1us axi4_drive_h.read_dword(8);
end
```

# Axi-stream
## task
* task write(int len, int start, bit[07:00] settings)

## example
```verilog
`include "./axi_sim/axi_stream_bfm.svh"
`include "./axi_sim/axi_stream_drive.svh"

axi_stream_bfm #
(
    .WIDTH  (8)
)
axi_stream_if 
(
    .s_aclk       (    clk      ),
    .s_aresetn    (    rst_n    )
);

//axi_stream_drive #(.width (8) ) axi_stream_drive_h;
axi_stream_drive #(8) axi_stream_drive_h;

int frame[];

initial
begin
    axi_stream_drive_h = new(axi_stream_if);
    frame = new[255];
    for(int i = 0; i < 255; i = i + 1 )
    begin
        frame[i] = i+1;
    end
    #5us axi_stream_drive_h.write(frame, 10, FALSE);
end
```

# uart
```verilog
uart_bfm #
(
    .BAUDRATE  (115200                        )
)
uart_bfm_if ();

uart_drive #(115200) uart_drive_h;

initial begin
    uart_drive_h = new(uart_bfm_if);
    #200us uart_drive_h.send_byte(8'h55    );
    #20us uart_drive_h.send_byte(8'haa    );
end
```
