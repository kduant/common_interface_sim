quit -sim
# 可以不手动建立library
#vlib    work
#vmap    work work


#工程所需要的文件
vlog -incr ./filter.v
vlog -incr ./filter_tb.v

vsim -t ns -novopt  work.filter_tb

log -r /*
radix 16

do wave.do

run 20us
