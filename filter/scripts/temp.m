Fs = 48000;
dt = 1.0/Fs;
T = 0.01;
f_sig = 1000;
f_noise = 5000;
N = T/dt;

n = linspace(0, T, N);
sig = sin(2*pi*f_sig*n);
noise = sin(2*pi*f_noise*n);
s = sig + noise;

subplot(3,1,1);
plot( n, sig)

subplot(3,1,2);
plot( n, noise)

subplot(3,1,3);
plot( n, s)
grid on

IN_SCALE = 14;% input scaling length

% quantize
signal_scale = round(s * 2^IN_SCALE);

WIDTH = 16;

signal_trans2c = dec2bin(signal_scale + 2^WIDTH * (signal_scale<0) , WIDTH);

signal_trans2c = signal_trans2c';
fdata = fopen('databin.mem' , 'wb');

for index = 1:length(signal_scale)
    for i = 1:WIDTH 
        fprintf( fdata ,'%s' , signal_trans2c((index-1) * WIDTH + i));
    end
    fprintf(fdata , '\r\n'); % entering a enter and new a line
end
fclose(fdata);

