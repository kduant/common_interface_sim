quit -sim
# 可以不手动建立library
#vlib    work
#vmap    work work


#工程所需要的文件
vlog -sv -incr ../bench/fir_filter_tb.sv
vlog -incr ../hdl/fir_direct_I.v

vsim -t ns -novopt  work.fir_filter_tb

log -r /*
radix 16

do wave.do

run 20us
