`timescale  1 ns/1 ps

module fir_filter_tb ();
reg                     clk = 0;
always
    #(1s/100_000_000/2) clk = ~clk;

reg                     rst = 0;
initial
begin
    rst = 0; #1us;
    rst = 1; #1us; 
    rst = 0; #1us; 
end

reg signed[15:0] mem[241:0];
// read data from disk
initial begin
    $readmemb("../scripts/databin.mem" , mem);
end

logic   [15:00]             data_in = 0;

initial
begin
    #2us;
    for(int i = 0; i < 241; i = i + 1 )
    begin
        data_in = mem[i];
        @ (posedge clk);
    end
    #1us;$stop();
end

fir_direct_I fir_direct_IEx01
(
    .clk           (  clk        ),
    .rst           (  rst        ),
    .filter_in     (  data_in    ),
    .filter_out    (             )
);
endmodule
