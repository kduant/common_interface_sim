quit -sim
# 可以不手动建立library
#vlib    work
#vmap    work work


#工程所需要的文件
vlog -sv -incr ./sim_motor_driver.sv

vsim -t ns -voptargs=+acc=rn  work.sim_motor_driver

log -r /*
radix 16

do wave.do

run 100ms

