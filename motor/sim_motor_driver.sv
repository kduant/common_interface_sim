`timescale  1 ns/1 ps

module sim_motor_driver ();

`include "./motor_bfm.sv"
`include "./motor_driver.sv"


motor_bfm motor_bfm_i();
motor_driver motor_driver_h;

initial
begin
    motor_driver_h = new(motor_bfm_i);
    motor_driver_h.motor_config(163840, 1500);
    motor_driver_h.turn_round(1, 163840*50);
end

endmodule
