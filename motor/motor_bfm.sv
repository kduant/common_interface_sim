/*=============================================================================
# FileName    :	motor_bfm.sv
# Author      :	author
# Email       :	email@email.com
# Description :	

正转
        ____      ____      ____      ____     
A: ____|    |____|    |____|    |____|    |____
          ____      ____      ____      ____      
B:   ____|    |____|    |____|    |____|    |____ 

反转
            ____      ____      ____      ____     
A:     ____|    |____|    |____|    |____|    |____
          ____      ____      ____      ____      
B:   ____|    |____|    |____|    |____|    |____ 

# Version     :	1.0
# LastChange  :	2020-07-13 10:10:42
# ChangeLog   :	
=============================================================================*/
interface motor_bfm ();
/*
    wire xxx;
    logic xxx;
*/

    logic                       phase_a;
    logic                       phase_b;
    logic                       phase_z;

    initial
    begin
        phase_a = 0;
        phase_b = 0;
        phase_z = 0;
    end

endinterface
